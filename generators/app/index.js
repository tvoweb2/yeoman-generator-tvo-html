'use strict';
var yeoman = require('yeoman-generator');
var chalk = require('chalk');
var yosay = require('yosay');

module.exports = yeoman.generators.Base.extend({
  prompting: function () {
    var done = this.async();

    // Have Yeoman greet the user.
    this.log(yosay(
      'Welcome to the ultimate ' + chalk.red('TvoHtml') + ' generator!'
    ));

    var prompts = [
      {
        type: 'input',
        name: 'project_name',
        message: 'Project Name?',
        validate: function(val){
          return /[a-z0-9][-_a-z0-9]*/i.test(val);
        }
      }, {
        type: 'list',
        name: 'ogp_type',
        message: 'OGP-Type?',
        choices: ["website", "article", "blog"],
        default: 'website'
      }, {
        type: 'list',
        name: 'twitter_card',
        message: 'Twitter Card?',
        choices: ["summary", "summary_large_image", "photo", "gallery", "app", "player", "product", "None"],
        default: 'summary_large_image'
      }, {
        type: 'confirm',
        name: 'featurephone',
        message: 'Has featurePhone site?',
        default: false
      }, {
        type: 'checkbox',
        name: 'libraries',
        message: 'Install these libraries?',
        choices: ['Normalize.css','Font Awesome'],
        default: ['Normalize.css','Font Awesome']
      }, {
        type: 'checkbox',
        name: 'js_modules',
        message: 'Load TVO-JS modules?',
        choices: ['common','analyze','overlay','video','map','mobile','ui','util'],
        default: ['common','analyze']
      }, {
        type: 'confirm',
        name: 'legacy_html5css3',
        message: 'Legacy browser HTML5-CSS3 support? (html5shiv)',
        default: false
      }, {
        type: 'confirm',
        name: 'legacy_media',
        message: 'Legacy browser Video & Audio tag support? (html5media)',
        default: false
      }, {
        type: 'confirm',
        name: 'legacy_mediaqueries',
        message: 'Legacy browser Media-Queries support? (css3-mediaqueries)',
        default: false
      }, {
        type: 'confirm',
        name: 'sass',
        message: 'Use Sass/SCSS?',
        default: true
      }, {
        type: 'confirm',
        name: 'script',
        message: 'Use ES6/JSX and concat scripts? (with webpack)',
        default: true
      }, {
        type: 'confirm',
        name: 'imagemin',
        message: 'Optimize images?',
        default: true
      }, {
        type: 'list',
        name: 'tpl',
        message: 'Use HTML template?',
        choices: ["EJS", "None"],
        default: "EJS"
      },
    ];

    this.prompt(prompts, function (props) {
      this.props = props;
      done();
    }.bind(this));
  },

  writing: {
    app: function () {
      this.fs.copyTpl(
        this.templatePath('_package.json'),
        this.destinationPath('package.json'),
        this.props
      );
      this.fs.copy(
        this.templatePath('.htaccess'),
        this.destinationPath('.htaccess')
      );
      this.fs.copyTpl(
        this.templatePath('.sentinel.yml'),
        this.destinationPath('.sentinel.yml'),
        this.props
      );
      this.fs.copyTpl(
        this.templatePath('_gitignore'),
        this.destinationPath('.gitignore'),
        this.props
      );

      this.mkdir('css');
      this.mkdir('js');
      this.mkdir('images');

      if(this.props.sass){
        this.directory(
          this.templatePath('sass'),
          this.destinationPath('.sass')
        );
      }
      if(this.props.script){
        this.mkdir('.jssrc');
      }
      if(this.props.imagemin){
        this.mkdir('.imgsrc');
      }
      if(this.props.tpl === "EJS"){
        this.mkdir('.ejs');
        this.mkdir('.ejsdata');
      }
    },

    projectfiles: function () {
      this.fs.copyTpl(
        this.templatePath('_skeleton.html'),
        this.destinationPath(this.props.tpl === "EJS" ? '.ejs/_skeleton.ejs' : '_skeleton.html'),
        this.props
      );
    }
  },

  install: function () {
    if(this.props.legacy_html5css3){
      this.npmInstall('html5shiv', {save:true});
    }
    if(this.props.legacy_media){
      this.npmInstall('html5media', {save:true});
    }
    if(this.props.legacy_mediaqueries){
      this.npmInstall('css3-mediaqueries-js', {save:true});
    }
    if(this.props.libraries){
      if(this.props.libraries.indexOf("Normalize.css") !== -1){
        this.npmInstall('normalize.css', {save:true});
      }
      if(this.props.libraries.indexOf("Font Awesome") !== -1){
        this.npmInstall('@fortawesome/fontawesome-free', {save:true});
      }
    }
    this.installDependencies();
  }
});
